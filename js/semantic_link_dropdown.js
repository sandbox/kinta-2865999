(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.editor_semantic_link = {
    attach: function (context, drupalSettings) {
      var semanticClasses = drupalSettings.editorSemanticLink.semanticLinkDropdown.microformatsClasses
      $(context).find('[data-drupal-selector="edit-semantic-class"]').once('selectSemanticClass').each(function () {
        $(this).change(function(){ 
          var semantic_class = $(this).val();
          var previous_class = $(context).find('[data-drupal-selector="edit-attributes-class"]').val()
          /* REGEXP SIMLIFICATION*/
          //semantic classes or string
          semantic_classes_or = Object.keys(semanticClasses).splice(1).join("|").trim("")
          //replace old microformats with empty spaces
          no_mf2 = previous_class.replace( new RegExp(semantic_classes_or,"gm"),"");
          //append choosen microformat to class removing extra whitespaces
          attr_class = (no_mf2 + " " + semantic_class).replace(/\s+/g, " ").trim()
          $($(context).find('[data-drupal-selector="edit-attributes-class"]')).val( attr_class )
        });
      });
    }
  }
})(jQuery, Drupal, drupalSettings );
