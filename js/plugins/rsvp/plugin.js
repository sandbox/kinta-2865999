CKEDITOR.plugins.add( 'rsvp',
{
  icons: 'rsvp',
  hidpi: true,
  init: function( editor )
  {
    editor.addCommand( 'rsvpDialog', new CKEDITOR.dialogCommand( 'rsvpDialog' ) );
    editor.ui.addButton( 'rsvp',
    {
      label: 'rsvp',
      command: 'rsvpDialog',
    });
  
      CKEDITOR.dialog.add( 'rsvpDialog', this.path + 'dialogs/rsvp.js' );
  },
  onLoad: function() {
    CKEDITOR.addCss(
      'data.p-rsvp:before {' +
      '  content: "rsvp:";' +
      '  color: #ccc;' +
      '  font-size: 50%;' +
      '}'+
      'data.p-rsvp {' +
      '  border-bottom: dashed 1px #ccc;' +
      '}'
    );
  }
});
