(function (Drupal) {

CKEDITOR.dialog.add('rsvpDialog', function(editor) {
   return {
      title: 'RSVP Properties',
      minWidth: 400,
      minHeight: 200,
      contents: [{
         id: 'tab-basic',
         label: Drupal.t('Basic Settings'),
         elements: [{
               type: 'select',
               id: 'values',
               label: Drupal.t('Value'),
               items: [
                  ['yes'],
                  ['no'],
                  ['maybe'],
                  ['interested']
               ],
               default: 'yes',
               setup: function(element) {
                  //Updating
                  this.setValue(element.getAttribute("value"));
               },
               commit: function(element) {
                  //Inserting
                  element.setAttribute('value', this.getValue());
               }
            },
            {
               type: 'text',
               id: 'text',
               label: Drupal.t('Explanation'),
               validate: CKEDITOR.dialog.validate.notEmpty(Drupal.t("RSVP explanation cannot be empty.")),
               setup: function(element) {
                  this.setValue(element.getText());
               },
               commit: function(element) {
                  element.setText(this.getValue());
               }

            },
            {
               type: 'fieldset',
               id: 'event_url_set',
               setup: function(element){
                 this.getElement().setAttribute( 'style', 'display:none' ); 
                 //tab_basic = CKEDITOR.dialog.getCurrent().definition.getContents('tab-basic');
                 //CKEDITOR.dialog.getCurrent().hidePage('tab-link');
               },
               children: [{
                     type: 'html',
                     html: Drupal.t('Optionally add event link url here (subsequent link edits will be done in link dialog).')
                  },
                  {
                     type: 'text',
                     id: 'event_preposition',
                     label: Drupal.t('Event preposition'),
                     default: Drupal.t('to'),
                     setup: function(element) {
                        this.setValue("");
                        this.disable();
                     }
                  },
                  {
                     type: 'text',
                     id: 'event_url',
                     label: Drupal.t('Event url'),
                     //validate: CKEDITOR.dialog.validate.notEmpty( "Event url field cannot be empty." )
                     setup: function(element) {
                        this.disable();
                     }
                  },
                  {
                     type: 'text',
                     id: 'event_url_text',
                     label: Drupal.t('Event url text'),
                     setup: function(element) {
                        this.disable();
                     }
                  }
               ]
            },
         ]
      }],
      onShow: function() {
         var selection = editor.getSelection();
         var element = selection.getStartElement();

         if (element)
            element = element.getAscendant(function(el) {
               if (!(el instanceof CKEDITOR.dom.element))
                  return false;
               data = el.getName();
               if (data == "data")
                  return el.hasClass('p-rsvp');
            }, true);

         if (!element || element.getName() != 'data') {
            element = editor.document.createElement('data');
            this.insertMode = true;
         } else
            this.insertMode = false;

         this.element = element;
         if (!this.insertMode)
            this.setupContent(this.element);
      },

      onOk: function() {
         var dialog = this;
         var rsvp = this.element;
         dialog.commitContent(rsvp);

         if (!dialog.insertMode)
            return;
         rsvp.setAttribute('class', 'p-rsvp');
         rsvp.setAttribute('value', dialog.getValueOf('tab-basic', 'values'));
         rsvp.setText(dialog.getValueOf('tab-basic', 'text'));

         var event_url = editor.document.createElement('a');
         event_url.setAttribute('href', dialog.getValueOf('tab-basic', 'event_url'));
         event_url.setAttribute('class', 'u-in-reply-to');
         var event_url_text = dialog.getValueOf('tab-basic', 'event_url_text');
         event_url_text = dialog.getValueOf('tab-basic', 'event_url_text') ? dialog.getValueOf('tab-basic', 'event_url_text') : dialog.getValueOf('tab-basic', 'event_url');
         event_url.setText(event_url_text);

         editor.insertElement(rsvp);
         editor.insertText(" " + dialog.getValueOf('tab-basic', 'event_preposition') + " ");
         editor.insertElement(event_url);
      }
   };
});
})(Drupal);
